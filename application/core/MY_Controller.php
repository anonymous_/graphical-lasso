<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller{

	public function __construct(){
		parent::__construct();
		$this->load->model('admin/M_user', 'm_user');
		if(empty($this->session->userdata('username'))){
			redirect('Auth');
		}
		
		$this->data['logout'] = "Admin/logoutSession";
		$this->data['url']	  = 'Admin';
	}
	
	public function getSession($user_id=null){
		if($user_id!=null && $user_id == $this->session->userdata('user_id')){
			echo json_encode($this->session->userdata());
		}else{
			redirect('Auth');
		}
	}
				
	public function logoutSession() {
		$this->session->sess_destroy();
		redirect('Auth');
	}

	function get(){
		$table = $_POST['table'];
		$query = $this->db->get($table);
		echo json_encode($query->result());
	}

	function getWhere(){
		$where = $_POST['where'];
		$table = $_POST['table'];
		$query = $this->db->where($where)->get($table);
		echo json_encode($query->result());
	}

}