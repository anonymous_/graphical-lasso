<?php
include FCPATH.'/vendor/autoload.php';
use JasperPHP\JasperPHP;
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function index(){
		$this->load->view('welcome_message');
	}

	function testJasper(){
		// echo json_encode(FCPATH.'/vendor/autoload.php');
		$fileInput  = base_url('/vendor/cossou/jasperphp/examples/hello_world.jrxml');
		$fileOutput = base_url('outputjasper/hello');;
		$formats    = array('pdf');
		$parameters = array('php_version' => '5.1.1');
		$db_connection = array(
		      'driver' => 'postgres',
		      'username' => 'vagrant',
		      'host' => 'localhost',
		      'database' => 'samples',
		      'port' => '5433',
		    );
		$background = true;
		$redirect_output = true;
  //     	// echo json_encode(phpversion());
  	   	
      	$jasper = new JasperPHP;
		$jasper->process($fileInput, $fileOutput, $formats, $parameters)->output();
		// echo json_encode($jasper);
		// JasperPHP::process($fileInput, $fileOutput, $formats, $parameters)->execute();
	}
}
