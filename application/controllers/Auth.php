<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Auth extends CI_Controller{
    function __construct() {
      parent::__construct();
      $this->load->model('admin/M_user');
      if ($this->session->userdata('username')) {
          if ($this->session->userdata('tipe_user') == 'admin') {
              redirect('Admin');
          }
      }
	}

	function index(){
		$data['title'] = "Login";
		$this->load->view('Admin/login', $data);
    }

    function proses(){
        $this->load->library('Authentication');
        $username = $this->input->post('username');
        $password = md5($this->input->post('password'));
        $data = $this->authentication->prosesLogin(['username' => $username, 'password' => $password], 'user');
        if($data['success']){
            if($data['data']->allow_login == 'Y'){
                $session_user['nama']      	     = $data['data']->nama;
                $session_user['username']   	 = $data['data']->username;
                $session_user['tipe_user']     	 = $data['data']->tipe_user;
                $this->session->set_userdata($session_user);
                if($this->db->where(["username" => $username, "tipe_user" => $data['data']->tipe_user, "allow_login" => "Y"])
                    ->update("user", ["last_login" => date('Y-m-d H:i:s')]))
                    redirect('Admin');
            }else{
                $this->session->set_flashdata('result_login', '<br>your account not allow login.');
                redirect('Auth');
            }
        }else{
            $this->session->set_flashdata('result_login', '<br>'.$data['message']);
            redirect('Auth');
        }
    }

    function phpInfo(){
        echo phpinfo();
    }

}


?>