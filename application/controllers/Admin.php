<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Admin extends MY_Controller{

	public function index(){
		$data = $this->data;
		$data['title'] 	 = 'Home';
		$data['content'] = 'home';
		$data['script']  = 'sc_home';
		$data['nilaiDosen']  = $this->m_user->getNilaiDosen()->result();
		$data['ganjil2019']  =  $this->m_user->getChartkategori1()->result();
		$data['genap2019'] 	 =  $this->m_user->getChartkategori2()->result();
		$data['ganjil2020']  =  $this->m_user->getChartkategori3()->result();
		$this->load->view('admin/container',$data);
	}
	
	public function dataDosen(){
		$data = $this->data;
		$data['title'] 	 = 'Data Dosen';
		$data['content'] = 'data_dosen';
		$data['script']  = 'sc_data_dosen';
		$this->load->view('admin/container',$data);
	}

	public function dataGraphical(){
		$data = $this->data;
		$data['title'] 	 = 'Data Graphical Lasso';
		$data['content'] = 'data_graphical';
		$data['script']  = 'sc_data_graphical';
		$this->load->view('admin/container',$data);
	}

	function getNilaiDosen(){
		$query = $this->m_user->getNilaiDosenSmt();
		echo json_encode($query->result());
	}

	function getNilaiDosenProsentase(){
		$query = $this->m_user->getDataDosenProsentase();
		echo json_encode($query->result());
	}

	function getPerDosen(){
		$query = $this->m_user->getNilaiPerDosen();
		echo json_encode($query->result());
	}

	function uploadExcel(){
		$path = FCPATH.'/assets/uploads/';
		$config['upload_path'] = $path;
		$config['allowed_types'] = 'xlsx';
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		
		if (!$this->upload->do_upload('fileToUpload')) {
			if(pathinfo($this->upload->data['file_name'], PATHINFO_BASENAME) != 'xlsx'){
				$this->session->set_flashdata('toast_tr', '<script type="text/javascript">toastr.error("Gagal Upload File, Check File Excel");</script>');
			}else{
				$this->session->set_flashdata('toast_tr', '<script type="text/javascript">toastr.error("Gagal Upload File, File not support");</script>');
			}
			redirect('Admin');
		} else {
			$data_upload  = $this->upload->data();
			$file = $data_upload['file_name'];
			require_once APPPATH . '/third_party/SimpleXLSX.php';
			// Cek apakah terdapat file data.xlsx pada folder tmp
			$inputFileName = $path . $file;
			ini_set('error_reporting', E_ALL );
			ini_set('display_errors', 1 );

			$xls = SimpleXLSX::parse($inputFileName, false, true );
			$data = array();
			$numrow = 1;
			foreach($xls->rows() as $row){
				if($numrow > 1){
					$nilai = ($row[6]+$row[7]+$row[8]+$row[9])/4;
					if($nilai > 4.5)
						$ket = "Sangat Baik";
					else if($nilai > 4)
						$ket = "Baik";
					else if($nilai>3)
						$ket = "Cukup";
					else if ($nilai>2)
						$ket = "Kurang Baik";
					else
						$ket = "Sangat Kurang Baik";

					array_push($data, array(
						'id_semester'		=> $row[0],
						'nip' 					=> $row[1],
						'nama'      		=> $row[2],
						'id_prodi'     	=> $row[3],
						'nm_prodi'     	=> $row[4],
						'id_fakultas'     		=> $row[5],
						'nilai_pendagogik'    => $row[6],
						'nilai_profesional'   => $row[7],
						'nilai_kepribadian'   => $row[8],
						'nilai_sosial'     		=> $row[9],
						'nilai'     		=> round($nilai, 2),
						'keterangan'    => $ket,
						'jumlah_data'   => "",
						'waktu_post'		=> date('Y-m-d H:i:s')
					));
				}
				$numrow++;
			}
			echo json_encode($data);
			die;
			if($this->db->insert_batch('tb_rekap_dosen', $data)){
				$this->session->set_flashdata('toast_tr', '<script type="text/javascript">toastr.success("Upload File berhasil");</script>');
			}else{
				$this->session->set_flashdata('toast_tr', '<script type="text/javascript">toastr.error("Gagal Insert Data, Check file excel");</script>');
			}
			unlink(realpath($inputFileName));
			redirect('Admin');
		}
	}

	function phpInfo(){
		$file = 'themplateImportExcelNew.xlsx';
		$path = FCPATH.'/assets/uploads/';
		// require_once APPPATH . "/third_party/PHPExcel/Classes/PHPExcel.php";

		require_once APPPATH . '/third_party/SimpleXLSX.php';
		// Cek apakah terdapat file data.xlsx pada folder tmp
		$inputFileName = $path . $file;
		ini_set('error_reporting', E_ALL );
		ini_set('display_errors', 1 );

		$xls = SimpleXLSX::parse($inputFileName, false, true );
		$data = array();
		$numrow = 1;
		foreach($xls->rows() as $row){
			if($numrow > 1){
				$nilai = ($row[6]+$row[7]+$row[8]+$row[9])/4;
				if($nilai > 4.5)
					$ket = "Sangat Baik";
				else if($nilai > 4)
					$ket = "Baik";
				else if($nilai>3)
					$ket = "Cukup";
				else if ($nilai>2)
					$ket = "Kurang Baik";
				else
					$ket = "Sangat Kurang Baik";

				array_push($data, array(
						'id_semester'		=> $row[0],
						'nip' 					=> $row[1],
						'nama'      		=> $row[2],
						'id_prodi'     	=> $row[3],
						'nm_prodi'     	=> $row[4],
						'id_fakultas'     		=> $row[5],
						'nilai_pendagogik'    => $row[6],
						'nilai_profesional'   => $row[7],
						'nilai_kepribadian'   => $row[8],
						'nilai_sosial'     		=> $row[9],
						'nilai'     		=> round($nilai, 2),
						'keterangan'    => $ket,
						'jumlah_data'   => "",
						'waktu_post'		=> date('Y-m-d H:i:s')
				));
			}
			$numrow++;
		}
		echo json_encode( $data );
		// print_r( $xls->sheets );
	}

}


?>