<?php

class Authentication{

	var $CI;
	public function __construct() {
		$this->CI =& get_instance();
		$this->CI->load->database();
	}
		 
	function prosesLogin($params, $table, $user=false){
		$data = [];
		$success = true;
		$message = '';
		if(!$user){
			$arraykeyuser = ['username', 'user', 'user_id', 'email'];
		}
		
		if($user){
			$username = [$user => $params[$user]];
			$validasi = $user;
		}else{
			if(in_array(array_keys($params)[0], $arraykeyuser , true)){
				$username = [array_keys($params)[0] => $params[array_keys($params)[0]]];
			}else{
				$username = [array_keys($params)[1] => $params[array_keys($params)[1]]];
			}
			$validasi = in_array(array_keys($username)[0], $arraykeyuser, true);
		}

		if($validasi){
			$db = $this->CI->db->where($username)->get($table);
			if($db->num_rows() == 0){
				$success = false;
				$message = 'your user account entered is incorrect.';
			}else{
				$db = $this->CI->db->where($params)->get($table);
				if($db->num_rows() == 0){
					$success = false;
					$message = 'your password entered is incorrect.';
				}else{
					$data = $db->row();
					$success = true;
					$message = 'login berhasil';
				}
			}
		}else{
			$db = $this->CI->db->where($params)->get($table);
			if($db->num_rows() == 0){
				$success = false;
				$message = 'your account entered is incorrect.';
			}else{
				$data = $db->row();
				$success = true;
				$message = 'login berhasil';
			}
		}
		return ['data' => $data, 'success' => $success, 'message' => $message];
	}
}