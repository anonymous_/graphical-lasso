<?php

class M_user extends CI_Model{

	function getNilaiDosen(){
		$query = "SELECT '1' id,'Nilai Pendagogik' nama, ROUND(SUM(rd.`nilai_pendagogik`)/COUNT(rd.`nip`),2) nilai,
					CASE
						WHEN rd.nilai_pendagogik > 4.5 THEN 'Sangat Baik'
						WHEN rd.nilai_pendagogik > 4 THEN 'Baik'
						WHEN rd.nilai_pendagogik > 3 THEN 'Cukup'
						WHEN rd.nilai_pendagogik > 2 THEN 'Kurang Baik'
						WHEN rd.nilai_pendagogik > 1 THEN 'Sangat Kurang Baik'
					END keterangan
					FROM tb_rekap_dosen rd
				UNION ALL
				SELECT '2' id, 'Nilai Profesional' nama, ROUND(SUM(rd.`nilai_profesional`)/COUNT(rd.`nip`),2) nilai,
					CASE
						WHEN rd.nilai_profesional > 4.5 THEN 'Sangat Baik'
						WHEN rd.nilai_profesional > 4 THEN 'Baik'
						WHEN rd.nilai_profesional > 3 THEN 'Cukup'
						WHEN rd.nilai_profesional > 2 THEN 'Kurang Baik'
						WHEN rd.nilai_profesional > 1 THEN 'Sangat Kurang Baik'
					END keterangan
					FROM tb_rekap_dosen rd
				UNION ALL
				SELECT '3' id, 'Nilai Kepribadian' nama, ROUND(SUM(rd.`nilai_kepribadian`)/COUNT(rd.`nip`),2) nilai,
					CASE
						WHEN rd.nilai_kepribadian > 4.5 THEN 'Sangat Baik'
						WHEN rd.nilai_kepribadian > 4 THEN 'Baik'
						WHEN rd.nilai_kepribadian > 3 THEN 'Cukup'
						WHEN rd.nilai_kepribadian > 2 THEN 'Kurang Baik'
						WHEN rd.nilai_kepribadian > 1 THEN 'Sangat Kurang Baik'
					END keterangan
					FROM tb_rekap_dosen rd
				UNION ALL
				SELECT '4' id, 'Nilai Sosial' nama, ROUND(SUM(rd.`nilai_sosial`)/COUNT(rd.`nip`),2) nilai,
					CASE
						WHEN rd.nilai_sosial > 4.5 THEN 'Sangat Baik'
						WHEN rd.nilai_sosial > 4 THEN 'Baik'
						WHEN rd.nilai_sosial > 3 THEN 'Cukup'
						WHEN rd.nilai_sosial > 2 THEN 'Kurang Baik'
						WHEN rd.nilai_sosial > 1 THEN 'Sangat Kurang Baik'
					END keterangan
					FROM tb_rekap_dosen rd";
		return $this->db->query($query);
	}

	function getNilaiDosenSmt(){
		$query = "SELECT rd.`id_semester`, s.`nm_semester` semester,
							ROUND(SUM(rd.`nilai_pendagogik`)/COUNT(rd.`nilai_pendagogik`), 2) nilai_pendagogik,
							ROUND(SUM(rd.`nilai_profesional`)/COUNT(rd.`nilai_profesional`), 2) nilai_profesional,
							ROUND(SUM(rd.`nilai_kepribadian`)/COUNT(rd.`nilai_kepribadian`), 2) nilai_kepribadian,
							ROUND(SUM(rd.`nilai_sosial`)/COUNT(rd.`nilai_sosial`), 2) nilai_sosial,
							ROUND((SUM(rd.`nilai_pendagogik`)/COUNT(rd.`nilai_pendagogik`)+
								SUM(rd.`nilai_profesional`)/COUNT(rd.`nilai_profesional`)+
								SUM(rd.`nilai_kepribadian`)/COUNT(rd.`nilai_kepribadian`)+
								SUM(rd.`nilai_sosial`)/COUNT(rd.`nilai_sosial`))/4, 2) nilai,
							CASE 
									WHEN ROUND((SUM(rd.`nilai_pendagogik`)/COUNT(rd.`nilai_pendagogik`)+
								SUM(rd.`nilai_profesional`)/COUNT(rd.`nilai_profesional`)+
								SUM(rd.`nilai_kepribadian`)/COUNT(rd.`nilai_kepribadian`)+
								SUM(rd.`nilai_sosial`)/COUNT(rd.`nilai_sosial`))/4, 2) > 4.5 THEN 'Sangat Baik'
									WHEN ROUND((SUM(rd.`nilai_pendagogik`)/COUNT(rd.`nilai_pendagogik`)+
								SUM(rd.`nilai_profesional`)/COUNT(rd.`nilai_profesional`)+
								SUM(rd.`nilai_kepribadian`)/COUNT(rd.`nilai_kepribadian`)+
								SUM(rd.`nilai_sosial`)/COUNT(rd.`nilai_sosial`))/4, 2) > 4 THEN 'Baik'
									WHEN ROUND((SUM(rd.`nilai_pendagogik`)/COUNT(rd.`nilai_pendagogik`)+
								SUM(rd.`nilai_profesional`)/COUNT(rd.`nilai_profesional`)+
								SUM(rd.`nilai_kepribadian`)/COUNT(rd.`nilai_kepribadian`)+
								SUM(rd.`nilai_sosial`)/COUNT(rd.`nilai_sosial`))/4, 2) > 3 THEN 'Cukup'
									WHEN ROUND((SUM(rd.`nilai_pendagogik`)/COUNT(rd.`nilai_pendagogik`)+
								SUM(rd.`nilai_profesional`)/COUNT(rd.`nilai_profesional`)+
								SUM(rd.`nilai_kepribadian`)/COUNT(rd.`nilai_kepribadian`)+
								SUM(rd.`nilai_sosial`)/COUNT(rd.`nilai_sosial`))/4, 2) > 2 THEN 'Kurang Baik'
									WHEN ROUND((SUM(rd.`nilai_pendagogik`)/COUNT(rd.`nilai_pendagogik`)+
								SUM(rd.`nilai_profesional`)/COUNT(rd.`nilai_profesional`)+
								SUM(rd.`nilai_kepribadian`)/COUNT(rd.`nilai_kepribadian`)+
								SUM(rd.`nilai_sosial`)/COUNT(rd.`nilai_sosial`))/4, 2) > 1 THEN 'Sangat Kurang Baik'
							END keterangan 
								FROM tb_rekap_dosen rd
							LEFT JOIN tb_semester s ON s.`id_semester` = rd.`id_semester`
								GROUP BY rd.`id_semester`";
		return $this->db->query($query);
	}

	function getNilaiPerDosen(){
		$query = "SELECT rd.`id_semester`, rd.nip, 
					ROUND((rd.`nilai_pendagogik`+rd.`nilai_profesional`+rd.`nilai_kepribadian`+rd.`nilai_sosial`)/4, 2) j_nilai,
					CASE 
							WHEN ROUND((rd.`nilai_pendagogik`+rd.`nilai_profesional`+rd.`nilai_kepribadian`+rd.`nilai_sosial`)/4, 2) > 4.5 THEN 'Sangat Baik'
							WHEN ROUND((rd.`nilai_pendagogik`+rd.`nilai_profesional`+rd.`nilai_kepribadian`+rd.`nilai_sosial`)/4, 2) > 4 THEN 'Baik'
							WHEN ROUND((rd.`nilai_pendagogik`+rd.`nilai_profesional`+rd.`nilai_kepribadian`+rd.`nilai_sosial`)/4, 2) > 3 THEN 'Cukup'
							WHEN ROUND((rd.`nilai_pendagogik`+rd.`nilai_profesional`+rd.`nilai_kepribadian`+rd.`nilai_sosial`)/4, 2) > 2 THEN 'Kurang Baik'
							WHEN ROUND((rd.`nilai_pendagogik`+rd.`nilai_profesional`+rd.`nilai_kepribadian`+rd.`nilai_sosial`)/4, 2) > 1 THEN 'Sangat Kurang Baik'
					END keterangan
						FROM tb_rekap_dosen rd
					WHERE rd.id_semester = (SELECT id_semester FROM tb_semester WHERE STATUS = 1) LIMIT 30";
		return $this->db->query($query);
	}

	function getDataDosenProsentase(){
		$query = "SELECT s.`nm_semester`, rd.`nip`, rd.`nama`, rd.`nm_prodi`, 
						ROUND((rd.`nilai_kepribadian`+rd.`nilai_pendagogik`+rd.`nilai_profesional`+rd.`nilai_sosial`)/4, 2) nilai,
						CASE
								WHEN ROUND((rd.`nilai_kepribadian`+rd.`nilai_pendagogik`+rd.`nilai_profesional`+rd.`nilai_sosial`)/4, 2) > 4.5 THEN 'Sangat Baik'
								WHEN ROUND((rd.`nilai_kepribadian`+rd.`nilai_pendagogik`+rd.`nilai_profesional`+rd.`nilai_sosial`)/4, 2) > 4 THEN 'Baik'
								WHEN ROUND((rd.`nilai_kepribadian`+rd.`nilai_pendagogik`+rd.`nilai_profesional`+rd.`nilai_sosial`)/4, 2) > 3 THEN 'Cukup'
								WHEN ROUND((rd.`nilai_kepribadian`+rd.`nilai_pendagogik`+rd.`nilai_profesional`+rd.`nilai_sosial`)/4, 2) > 2 THEN 'Kurang Baik'
								WHEN ROUND((rd.`nilai_kepribadian`+rd.`nilai_pendagogik`+rd.`nilai_profesional`+rd.`nilai_sosial`)/4, 2) > 1 THEN 'Sangat Kurang Baik'
								ELSE 'Sangat Kurang Baik'
						END keterangan,
						ROUND(((rd.`nilai_kepribadian`+rd.`nilai_pendagogik`+rd.`nilai_profesional`+rd.`nilai_sosial`)/4)/5*100, 0) prosentase
							FROM tb_rekap_dosen rd
								JOIN tb_semester s ON s.`id_semester` = rd.`id_semester`";
		return $this->db->query($query);
	}
	function getChartkategori1(){
		$query = "SELECT s.`nm_semester`, rd.`nip`, rd.`nama`, rd.`nm_prodi`, 
						ROUND((rd.`nilai_kepribadian`+rd.`nilai_pendagogik`+rd.`nilai_profesional`+rd.`nilai_sosial`)/4, 2) nilai,
						CASE
								WHEN ROUND((rd.`nilai_kepribadian`+rd.`nilai_pendagogik`+rd.`nilai_profesional`+rd.`nilai_sosial`)/4, 2) > 4.5 THEN 'Sangat Baik'
								WHEN ROUND((rd.`nilai_kepribadian`+rd.`nilai_pendagogik`+rd.`nilai_profesional`+rd.`nilai_sosial`)/4, 2) > 4 THEN 'Baik'
								WHEN ROUND((rd.`nilai_kepribadian`+rd.`nilai_pendagogik`+rd.`nilai_profesional`+rd.`nilai_sosial`)/4, 2) > 3 THEN 'Cukup'
								WHEN ROUND((rd.`nilai_kepribadian`+rd.`nilai_pendagogik`+rd.`nilai_profesional`+rd.`nilai_sosial`)/4, 2) > 2 THEN 'Kurang Baik'
								WHEN ROUND((rd.`nilai_kepribadian`+rd.`nilai_pendagogik`+rd.`nilai_profesional`+rd.`nilai_sosial`)/4, 2) > 1 THEN 'Sangat Kurang Baik'
								ELSE 'Sangat Kurang Baik'
						END keterangan,
						ROUND(((rd.`nilai_kepribadian`+rd.`nilai_pendagogik`+rd.`nilai_profesional`+rd.`nilai_sosial`)/4)/5*100, 0) prosentase
							FROM tb_rekap_dosen rd
								JOIN tb_semester s ON s.`id_semester` = rd.`id_semester`
								WHERE s.id_semester = '20181'
								";
		return $this->db->query($query);
	}
	function getChartkategori2(){
		$query = "SELECT s.`nm_semester`, rd.`nip`, rd.`nama`, rd.`nm_prodi`, 
						ROUND((rd.`nilai_kepribadian`+rd.`nilai_pendagogik`+rd.`nilai_profesional`+rd.`nilai_sosial`)/4, 2) nilai,
						CASE
								WHEN ROUND((rd.`nilai_kepribadian`+rd.`nilai_pendagogik`+rd.`nilai_profesional`+rd.`nilai_sosial`)/4, 2) > 4.5 THEN 'Sangat Baik'
								WHEN ROUND((rd.`nilai_kepribadian`+rd.`nilai_pendagogik`+rd.`nilai_profesional`+rd.`nilai_sosial`)/4, 2) > 4 THEN 'Baik'
								WHEN ROUND((rd.`nilai_kepribadian`+rd.`nilai_pendagogik`+rd.`nilai_profesional`+rd.`nilai_sosial`)/4, 2) > 3 THEN 'Cukup'
								WHEN ROUND((rd.`nilai_kepribadian`+rd.`nilai_pendagogik`+rd.`nilai_profesional`+rd.`nilai_sosial`)/4, 2) > 2 THEN 'Kurang Baik'
								WHEN ROUND((rd.`nilai_kepribadian`+rd.`nilai_pendagogik`+rd.`nilai_profesional`+rd.`nilai_sosial`)/4, 2) > 1 THEN 'Sangat Kurang Baik'
								ELSE 'Sangat Kurang Baik'
						END keterangan,
						ROUND(((rd.`nilai_kepribadian`+rd.`nilai_pendagogik`+rd.`nilai_profesional`+rd.`nilai_sosial`)/4)/5*100, 0) prosentase
							FROM tb_rekap_dosen rd
								JOIN tb_semester s ON s.`id_semester` = rd.`id_semester`
								WHERE s.id_semester = '20182'
								";
		return $this->db->query($query);
	}
	function getChartkategori3(){
		$query = "SELECT s.`nm_semester`, rd.`nip`, rd.`nama`, rd.`nm_prodi`, 
						ROUND((rd.`nilai_kepribadian`+rd.`nilai_pendagogik`+rd.`nilai_profesional`+rd.`nilai_sosial`)/4, 2) nilai,
						CASE
								WHEN ROUND((rd.`nilai_kepribadian`+rd.`nilai_pendagogik`+rd.`nilai_profesional`+rd.`nilai_sosial`)/4, 2) > 4.5 THEN 'Sangat Baik'
								WHEN ROUND((rd.`nilai_kepribadian`+rd.`nilai_pendagogik`+rd.`nilai_profesional`+rd.`nilai_sosial`)/4, 2) > 4 THEN 'Baik'
								WHEN ROUND((rd.`nilai_kepribadian`+rd.`nilai_pendagogik`+rd.`nilai_profesional`+rd.`nilai_sosial`)/4, 2) > 3 THEN 'Cukup'
								WHEN ROUND((rd.`nilai_kepribadian`+rd.`nilai_pendagogik`+rd.`nilai_profesional`+rd.`nilai_sosial`)/4, 2) > 2 THEN 'Kurang Baik'
								WHEN ROUND((rd.`nilai_kepribadian`+rd.`nilai_pendagogik`+rd.`nilai_profesional`+rd.`nilai_sosial`)/4, 2) > 1 THEN 'Sangat Kurang Baik'
								ELSE 'Sangat Kurang Baik'
						END keterangan,
						ROUND(((rd.`nilai_kepribadian`+rd.`nilai_pendagogik`+rd.`nilai_profesional`+rd.`nilai_sosial`)/4)/5*100, 0) prosentase
							FROM tb_rekap_dosen rd
								JOIN tb_semester s ON s.`id_semester` = rd.`id_semester`
								WHERE s.id_semester = '20191'
								";
		return $this->db->query($query);
	}
}

?>