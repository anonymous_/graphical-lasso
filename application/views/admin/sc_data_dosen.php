<!-- DataTables -->
<script src="assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="assets/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<!-- AdminLTE App -->

<script>
	var tableData;
	var dataSet = [];
	$(function () {
		tableData = $("#tb_dosen").DataTable({
			"responsive": true,
			"autoWidth": false,
			data: dataSet,
            columns: [
               { title: "Nip" },
               { title: "Nama" },
               { title: "Prodi" },
			   { title: "Nilai" },
               { title: "Keterangan" }
            ]
		});
		blockUI("#tb_dosen");
		$.post('Admin/get', {table:'tb_rekap_dosen'}, function( data ) {
			tableData.clear();
			dataSet = [];
			$.each(data, function(key, parent) {
				dataSet.push([parent.nip, parent.nama, parent.nm_prodi, parent.nilai, parent.keterangan]);
			});
			tableData.rows.add(dataSet);
			tableData.draw();
			$("#tb_dosen").unblock();
      	},"json");
	});
</script>