<section class="content">
  <div class="container-fluid">
    <!-- Info boxes -->
    <div class="row">
      
      <?php foreach ($nilaiDosen as $data){ ?>
      <div class="col-12 col-sm-6 col-md-3">
        <div class="info-box">
          <!-- <span class="info-box-icon bg-info elevation-1"><i class="fas fa-cog"></i></span> -->

          <div class="info-box-content">
            <span class="info-box-text"><?= $data->nama ?></span>
            <span class="info-box-number"><?= $data->nilai ?> </span>
            <small><?= $data->keterangan ?></small>
          </div>
          <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
      </div>
      <!-- /.col -->
      <?php } ?>
      <!-- /.col -->
    </div>
    <!-- /.row -->

    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            <h5 class="card-title">Penilaian Dosen</h5>

            <div class="card-tools">
              <button type="button" class="btn btn-xs btn-success" id="btn-download">
                Download Themplate Excel
              </button>
              <button type="button" class="btn btn-xs btn-primary" data-toggle="modal" data-target="#modal-upload-excel">
                Import Excel
              </button>
              <button type="button" class="btn btn-tool" data-card-widget="collapse">
                <i class="fas fa-minus"></i>
              </button>
            </div>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <div class="row">
              <div class="col-md-12">
                <!-- <p class="text-center">
                  <strong>Sales: 1 Jan, 2014 - 30 Jul, 2014</strong>
                </p> -->

                <div id="chartdiv" style="height: 500px;">
                
                </div>
               <!--   <div class="chart">
                  <canvas id="persemesterChart" height="100" style="height: 100px;"></canvas>
                </div> -->
              </div>
            </div>
            <!-- /.row -->
          </div>
        </div>
        <!-- /.card -->
      </div>
      <!-- /.col -->
    </div>

    <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">List Data Dosen</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
          <table id="tb_dosen" class="table table-bordered table-striped">
          </table>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
    </div>
    <!-- /.col -->
  </div>
    
    
  </div><!--/. container-fluid -->
</section>

<div class="modal fade" id="modal-upload-excel">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Import Data Excel</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form role="form" action="Admin/uploadExcel" method="post" enctype="multipart/form-data">
            <div class="card-body">
              <div class="form-group">
                <div class="input-group">
                  <div class="custom-file">
                    <input type="file" name="fileToUpload" id="fileToUpload" class="custom-file-input" required="required">
                    <label class="custom-file-label" for="fileToUpload">Choose file</label>
                  </div>
                  <div class="input-group-append">
                    <input type="submit" class="input-group-text" value="Upload" />
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
</div>