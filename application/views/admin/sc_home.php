<script src="assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="assets/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="https://www.amcharts.com/lib/4/core.js"></script>
<script src="https://www.amcharts.com/lib/4/charts.js"></script>
<script src="https://www.amcharts.com/lib/4/plugins/forceDirected.js"></script>
<script src="https://www.amcharts.com/lib/4/themes/animated.js"></script>

<script>
	var tableData;
	var dataSet = [];
	$(function () {
		bsCustomFileInput.init();
		$('#btn-download').on('click', function(){
			// toastr.info('Download themplate excel');
			window.location.href = window.location.origin+'/assets/download/themplateImportExcel.xlsx';
		});

		tableData = $("#tb_dosen").DataTable({
			"responsive": true,
			"autoWidth": false,
			data: dataSet,
			columns: [
			{ title: "Semester" },
			{ title: "Nip" },
			{ title: "Nama" },
			{ title: "Prodi" },
			{ title: "Nilai" },
			{ title: "Keterangan" },
			{ title: "Prosentase" }
			]
		});
		blockUI("#tb_dosen");
		$.get('Admin/getNilaiDosenProsentase', function( data ) {
			tableData.clear();
			dataSet = [];
			$.each(data, function(key, parent) {
				dataSet.push([parent.nm_semester, parent.nip, parent.nama, parent.nm_prodi, parent.nilai, parent.keterangan, parent.prosentase+" %"]);
			});
			tableData.rows.add(dataSet);
			tableData.draw();
			$("#tb_dosen").unblock();
		},"json");
	});
</script>


<script>
	function createConfig(label, pendagogik, profesional, kepribadian, sosial) {
		return {
			type: 'line',
			data: {
				labels: label,
				datasets: [{
					label: 'Pendagogik',
					backgroundColor: window.chartColors.red,
					borderColor: window.chartColors.red,
					data: pendagogik,
					fill: false,
				}, {
					label: 'Profesional',
					fill: false,
					backgroundColor: window.chartColors.blue,
					borderColor: window.chartColors.blue,
					data: profesional,
				}, {
					label: 'Kepribadian',
					fill: false,
					backgroundColor: window.chartColors.yellow,
					borderColor: window.chartColors.yellow,
					data: kepribadian,
				}, {
					label: 'Sosial',
					fill: false,
					backgroundColor: window.chartColors.green,
					borderColor: window.chartColors.green,
					data: sosial,
				}]
			},
			options: {
				responsive: true,
				title: {
					display: true,
					text: "Penilaian Kinerja Dosen"
				},
				scales: {
					xAxes: [{
						gridLines: {display: true}
					}],
					yAxes: [{
						gridLines: {display: true},
						ticks: {
							min: 1,
							max: 5,
							stepSize: 0.05
						}
					}]
				}
			}
		};
	}

	am4core.ready(function() {

		// Themes begin
		am4core.useTheme(am4themes_animated);
		// Themes end



		var chart = am4core.create("chartdiv", am4plugins_forceDirected.ForceDirectedTree);
		var networkSeries = chart.series.push(new am4plugins_forceDirected.ForceDirectedSeries())

		chart.data = [
		{
			name: "Penilaian Dosen",
			children: [
			{
				name: "20181",
				children: [
					{ name: '1', children: [
							{ value: '4.5' },
							{ value: '4.4' },
							{ value: '4.0' },
							{ value: '4.2' },
							{ value: '4.2' },
							{ value: '4.1' },
						] 
					},
					{ name: '251061', children: [
							{ value: '4.2' },
							{ value: '4.1' },
							{ value: '4.2' },
							{ value: '4.2' },
							{ value: '4.1' },
						] 
					},
					{ name: '251075', children: [
							{ value: '4.2' },
							{ value: '4.2' },
							{ value: '4.3' },
							{ value: '4.2' },
							{ value: '4.1' },
							{ value: '4.1' },
							{ value: '4.0' },
							{ value: '4.0' },
						] 
					},
				]
			},
			{
				name: "20182",
				children: [
					{ name: '251083', children: [
							{ value: '4.2' },
							{ value: '4.2' },
						] 
					},{ name: '251091', children: [
							{ value: '4.0' },
							{ value: '4.0' },
							{ value: '4.3' },
							{ value: '4.6' },
						] 
					},{ name: '251092', children: [
							{ value: '4.4' },
							{ value: '4.4' },
							{ value: '4.2' },
							{ value: '4.1' },
							{ value: '4.1' },
							{ value: '4.3' },
						] 
					},{ name: '251061', children: [
							{ value: '4.3' },
							{ value: '4.3' },
							{ value: '4.2' },
							{ value: '4.1' },
							{ value: '4.0' },
							{ value: '4.0' },
							{ value: '4.3' },
							{ value: '4.5' },
							{ value: '4.2' },
						] 
					},
				]
			},
			{
				name: "20191",
				children: [
					{ name: '251142', children: [
							{ value: '3.4' },
							{ value: '3.8' },
							{ value: '4.1' },
						] 
					},{ name: '251156', children: [
							{ value: '4.1' },
							{ value: '4.3' },
							{ value: '4.0' },
							{ value: '4.3' },
							{ value: '4.1' },
						] 
					},{ name: '251164', children: [
							{ value: '3.5' },
						] 
					},
				]
			}


			]
		}


		];

		networkSeries.dataFields.value = "value";
		networkSeries.dataFields.name  = "value";
		networkSeries.dataFields.children = "children";
		networkSeries.nodes.template.tooltipText = "{value}";
		networkSeries.nodes.template.fillOpacity = 1;

		networkSeries.nodes.template.label.text = "{name}"
		networkSeries.fontSize = 10;

		networkSeries.links.template.strokeWidth = 1;

		var hoverState = networkSeries.links.template.states.create("hover");
		hoverState.properties.strokeWidth = 3;
		hoverState.properties.strokeOpacity = 1;

		networkSeries.nodes.template.events.on("over", function(event) {
			event.target.dataItem.childLinks.each(function(link) {
				link.isHover = true;
			})
			if (event.target.dataItem.parentLink) {
				event.target.dataItem.parentLink.isHover = true;
			}

		})

		networkSeries.nodes.template.events.on("out", function(event) {
			event.target.dataItem.childLinks.each(function(link) {
				link.isHover = false;
			})
			if (event.target.dataItem.parentLink) {
				event.target.dataItem.parentLink.isHover = false;
			}
		})

	});


	// window.onload = function() {
	// 	var ctx = document.getElementById('persemesterChart').getContext('2d');
	// 	$.get('Admin/getNilaiDosen', function( data ) {
	// 		var pen = [], pro = [], kep = [], sos = [], label = [];
	// 		$.each(data, function(index, itemData){
	// 			label.push(itemData.semester);
	// 			pen.push(itemData.nilai_pendagogik);
	// 			pro.push(itemData.nilai_profesional);
	// 			kep.push(itemData.nilai_kepribadian);
	// 			sos.push(itemData.nilai_sosial);
	// 		});
	// 		var config = createConfig(label, pen, pro, kep, sos);
	// 		new Chart(ctx, config);
	// 	},"json");

		// var options = {
		// 	legend: false,
		// 	tooltips: false,
		// 	elements: {
		// 		rectangle: {
		// 			backgroundColor: window.chartColors.blue
		// 		}
		// 	},
		// 	responsive: true,
		// 	title: {
		// 		display: true,
		// 		text: "Penilaian Kinerja Per Dosen"
		// 	},
		// 	scales: {
		// 		xAxes: [{
		// 			gridLines: {display: true}
		// 		}],
		// 		yAxes: [{
		// 			gridLines: {display: true},
		// 			ticks: {
		// 				min: 1,
		// 				max: 5,
		// 				stepSize: 0.05
		// 			}
		// 		}]
		// 	}
		// };

		// function getdataperDosen(label, data){
		// 	return {
		// 		labels: label,
		// 		datasets: [{
		// 			data: data,
		// 		}]
		// 	}
		// };

		// $.get('Admin/getPerDosen', function( data ) {
		// 	var row = [], label = [];
		// 	$.each(data, function(index, itemData){
		// 		label.push(itemData.nip);
		// 		row.push(itemData.j_nilai);
		// 	});
		// 	var chart = new Chart('perdosenChart', {
		// 		type: 'bar',
		// 		data: getdataperDosen(label, row),
		// 		options: options
		// 	});
		// },"json");

	// };
</script>