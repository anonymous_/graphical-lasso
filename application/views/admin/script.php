<script src="assets/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>

<script src="assets/plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>
<!-- overlayScrollbars -->
<script src="assets/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="assets/plugins/toastr/toastr.min.js"></script>

<script src="assets/dist/js/adminlte.js"></script>
<script src="assets/dist/js/md5.js"></script>
<!-- OPTIONAL SCRIPTS -->
<script src="assets/dist/js/demo.js"></script>

<!-- PAGE PLUGINS -->
<!-- jQuery Mapael -->
<script src="assets/plugins/jquery-mousewheel/jquery.mousewheel.js"></script>
<script src="assets/plugins/raphael/raphael.min.js"></script>
<script src="assets/plugins/jquery-mapael/jquery.mapael.min.js"></script>
<script src="assets/plugins/jquery-mapael/maps/usa_states.min.js"></script>
<!-- ChartJS -->
<script src="assets/plugins/chart.js/Chart.min.js"></script>

<!-- PAGE SCRIPTS -->
<script src="assets/dist/js/jquery.blockUI.js"></script>
<?php if (validation_errors() || $this->session->flashdata('toast_tr')) { ?>
	<?php echo $this->session->flashdata('toast_tr'); ?>
<?php } ?>
<script>
	$(document).ready(function () {
		bsCustomFileInput.init();
	});
	window.chartColors = {
    red: 'rgb(255, 99, 132)',
    orange: 'rgb(255, 159, 64)',
    yellow: 'rgb(255, 205, 86)',
    green: 'rgb(75, 192, 192)',
    blue: 'rgb(54, 162, 235)',
    purple: 'rgb(153, 102, 255)',
    grey: 'rgb(201, 203, 207)'
	};

	function blockUI(param){
		$(param).block({ 
			message: 'Please Wait....',
			overlayCSS: {
				backgroundColor: '#fff',
				opacity: 0.8,
				cursor: 'wait',
				'box-shadow': '0 0 0 1px #ddd'
			},
			css: {
				border: 0,
				padding: 0,
				backgroundColor: 'none'
			}
		});
	}

	$(function () {
		$('.change-password').on('click', function(){
			if($('#password').val() == ""){
				toastr.error('Password tidak boleh kosong!')
			}else if($('#retype_password').val() != $('#password').val()){
				toastr.error('Password tidak sama, ulangi pergantian password')
			}else{
				var where = {
					'username' : $('#username').attr('data-name'),
					'allow_login' : 'Y',
					'tipe_user' : $('#tipe_user').attr('data-name'),
				}
				var data = {'password' : $.md5($('#password').val())};
				blockUI(".modal-content");

				$.post('Admin/update', {table:'user', where:where, data:data }, function( data ) {
					toastr.success('Update password berhasil');
					$(".modal-content").unblock();
					$('#modal-change-password').modal('hide');
					$('#password').val("");
					$('#retype_password').val("");
				},"json");
			}
			
		});
	});
</script>