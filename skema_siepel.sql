/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 5.5.5-10.1.40-MariaDB : Database - siepel
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `tb_evaluasi` */

DROP TABLE IF EXISTS `tb_evaluasi`;

CREATE TABLE `tb_evaluasi` (
  `id_evaluasi` int(10) NOT NULL,
  `npm` varchar(100) NOT NULL,
  `nip` varchar(100) NOT NULL,
  `id_kelas` varchar(100) DEFAULT NULL,
  `id_semester` int(10) DEFAULT NULL,
  `id_prodi` int(10) DEFAULT NULL,
  `id_fakultas` int(10) DEFAULT NULL,
  `id_indikator` int(10) DEFAULT NULL,
  `id_jenis_indikator` int(5) DEFAULT NULL,
  `nilai` int(5) DEFAULT NULL,
  PRIMARY KEY (`id_evaluasi`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `tb_indikator_penilaian` */

DROP TABLE IF EXISTS `tb_indikator_penilaian`;

CREATE TABLE `tb_indikator_penilaian` (
  `id_indikator` int(10) NOT NULL AUTO_INCREMENT,
  `nm_indikator` varchar(150) NOT NULL,
  `id_jenis_indikator` int(10) DEFAULT NULL,
  `nm_jenis_indikator` varchar(150) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_indikator`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;

/*Table structure for table `tb_jenis_indikator` */

DROP TABLE IF EXISTS `tb_jenis_indikator`;

CREATE TABLE `tb_jenis_indikator` (
  `id_jenis_indikator` int(10) NOT NULL AUTO_INCREMENT,
  `nm_jenis_indikator` varchar(150) NOT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_jenis_indikator`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Table structure for table `tb_rekap_dosen` */

DROP TABLE IF EXISTS `tb_rekap_dosen`;

CREATE TABLE `tb_rekap_dosen` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `id_semester` int(10) NOT NULL,
  `nip` varchar(150) NOT NULL,
  `nama` varchar(150) DEFAULT NULL,
  `id_prodi` int(10) DEFAULT NULL,
  `nm_prodi` varchar(150) DEFAULT NULL,
  `id_fakultas` int(10) DEFAULT NULL,
  `nilai_pendagogik` float DEFAULT NULL,
  `nilai_profesional` float DEFAULT NULL,
  `nilai_kepribadian` float DEFAULT NULL,
  `nilai_sosial` float DEFAULT NULL,
  `nilai` float DEFAULT NULL,
  `keterangan` varchar(150) DEFAULT NULL,
  `jumlah_data` int(10) DEFAULT NULL,
  `waktu_post` datetime DEFAULT NULL,
  `waktu_edit` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1940 DEFAULT CHARSET=latin1;

/*Table structure for table `tb_rekap_evaluasi_mahasiswa` */

DROP TABLE IF EXISTS `tb_rekap_evaluasi_mahasiswa`;

CREATE TABLE `tb_rekap_evaluasi_mahasiswa` (
  `id` int(10) DEFAULT NULL,
  `npm` varchar(150) DEFAULT NULL,
  `id_semester` int(10) DEFAULT NULL,
  `token` varchar(150) DEFAULT NULL,
  `waktu` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `tb_rekap_fakultas` */

DROP TABLE IF EXISTS `tb_rekap_fakultas`;

CREATE TABLE `tb_rekap_fakultas` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `id_semester` int(10) NOT NULL,
  `id_fakultas` int(10) NOT NULL,
  `nm_fakultas` varchar(150) DEFAULT NULL,
  `nm_singkat` varchar(100) DEFAULT NULL,
  `nilai` float DEFAULT NULL,
  `jumlah_kelas` int(10) DEFAULT NULL,
  `jumlah_dosen` int(10) DEFAULT NULL,
  `jumlah_mhs` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

/*Table structure for table `tb_rekap_prodi` */

DROP TABLE IF EXISTS `tb_rekap_prodi`;

CREATE TABLE `tb_rekap_prodi` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `id_semester` int(10) NOT NULL,
  `id_prodi` int(10) NOT NULL,
  `nm_prodi` varchar(150) NOT NULL,
  `jenjang` varchar(100) DEFAULT NULL,
  `id_fakultas` int(10) DEFAULT NULL,
  `nilai` float DEFAULT NULL,
  `jumlah_kelas` int(10) DEFAULT NULL,
  `jumlah_dosen` int(10) DEFAULT NULL,
  `jumlah_mhs` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=134 DEFAULT CHARSET=latin1;

/*Table structure for table `tb_saran` */

DROP TABLE IF EXISTS `tb_saran`;

CREATE TABLE `tb_saran` (
  `id_saran` int(10) NOT NULL AUTO_INCREMENT,
  `npm` varchar(150) NOT NULL,
  `nip` varchar(150) NOT NULL,
  `id_kelas` int(10) DEFAULT NULL,
  `id_semester` int(10) DEFAULT NULL,
  `saran` varchar(150) DEFAULT NULL,
  `hasil` varchar(100) DEFAULT NULL,
  `waktu` datetime DEFAULT NULL,
  PRIMARY KEY (`id_saran`)
) ENGINE=InnoDB AUTO_INCREMENT=372773 DEFAULT CHARSET=latin1;

/*Table structure for table `tb_semester` */

DROP TABLE IF EXISTS `tb_semester`;

CREATE TABLE `tb_semester` (
  `id_semester` int(10) NOT NULL AUTO_INCREMENT,
  `nm_semester` varchar(150) NOT NULL,
  `status` int(5) NOT NULL,
  PRIMARY KEY (`id_semester`)
) ENGINE=InnoDB AUTO_INCREMENT=20192 DEFAULT CHARSET=latin1;

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id_user` int(10) NOT NULL AUTO_INCREMENT,
  `nama` varchar(150) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `tipe_user` varchar(100) DEFAULT NULL,
  `kode_prodi` varchar(100) DEFAULT NULL,
  `kode_fakultas` varchar(100) DEFAULT NULL,
  `allow_login` varchar(50) DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `waktu_post` datetime DEFAULT NULL,
  `waktu_edit` datetime DEFAULT NULL,
  PRIMARY KEY (`id_user`,`username`)
) ENGINE=InnoDB AUTO_INCREMENT=97 DEFAULT CHARSET=latin1;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
